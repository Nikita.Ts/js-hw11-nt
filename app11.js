const pwForm = document.querySelector(".password-form");
const checkBtn = document.querySelector(".btn");
pwForm.addEventListener('click', (event) => {
    if (event.target.classList.contains("icon-password")) {
        if (event.target.classList.contains("fa-eye")) {
            event.target.classList.replace("fa-eye", "fa-eye-slash");
            event.target.parentElement.querySelector("input").type = "text";
        }
        else {
            event.target.className = event.target.classList.replace("fa-eye-slash", "fa-eye");
            event.target.parentElement.querySelector("input").type = "password";
        }
    }
});
const errorData = document.createElement("p");
errorData.textContent = "The same data must be entered";
errorData.classList.add("error");

checkBtn.addEventListener('click', () => {
    const firstInput = document.querySelector(".password")
    const confirmInput = document.querySelector(".confirm-password")
    if (firstInput.value === confirmInput.value) {
        errorData.remove();
        alert("You are welcome!")   
    } else {
        document.querySelector("button").before(errorData); 
    }
})